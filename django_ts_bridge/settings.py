DJANGO_TS_BRIDGE_EXCLUDED_APPS = ['rest_framework', 'wagtail.api.v2']
DJANGO_TS_BRIDGE_CHOICE_DEFINITION_IMPORT_PATH = 'ts/django/django-model-choices'
DJANGO_TS_BRIDGE_CHOICE_DEFINITION_OUTPUT_PATH = 'src/ts/django/django-model-choices.ts'
DJANGO_TS_BRIDGE_MODEL_DEFINITION_OUTPUT_PATH = 'src/ts/@types/django-models.d.ts'
DJANGO_TS_BRIDGE_URL_OUTPUT_PATH = 'src/ts/django/urls.json'
DJANGO_TS_BRIDGE_CHOICE_TYPE_VAR_NAME = 'Choices'
DJANGO_TS_BRIDGE_CHOICE_VAR_NAME = 'choices'
DJANGO_TS_BRIDGE_URLS_VAR_NAME = 'Urls'