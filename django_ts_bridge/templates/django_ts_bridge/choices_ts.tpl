{% load django_ts_bridge %}
{% for app_key, app in choices.items  %}
    {% for model_key, model in app.items  %}
        {% for choice_definition_key, choice_definition in model.items %}
            enum {{app_key}}{{model_key}}{{choice_definition_key}} {
                {% for choice_key, choice in choice_definition.items %}
                    {{choice_key}} = {{choice|js_literal|safe}},
                {% endfor %}
            }
        {% endfor %}
    {% endfor %}
{% endfor %}
/* eslint-disable @typescript-eslint/no-namespace */
export namespace {{ ts_type_var_name }} {
    {% for app_key, app in choices.items %}
        export namespace {{app_key}} {
            {% for model_key, model in app.items %}
                export namespace {{model_key}} {
                    {% for choice_key, choice_definition in model.items %}
                        export type {{choice_key}} = {{app_key}}{{model_key}}{{choice_key}};
                    {% endfor %}
                }
            {% endfor %}
        }
    {% endfor %}
}
/* eslint-enable @typescript-eslint/no-namespace */
export const {{ ts_var_name }} = {
    {% for app_key, app in choices.items %}
        {{app_key}}: {
            {% for model_key, model in app.items %}
                {{model_key}}: {
                    {% for choice_key, choice_definition in model.items %}
                        {{choice_key}}: {{app_key}}{{model_key}}{{choice_key}},
                    {% endfor %}
                },
            {% endfor %}
        },
    {% endfor %}
};